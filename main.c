#include <mpi.h>
#include "common/utility.h"
#include <float.h>
#include <string.h>

#ifndef BUF_SIZE
#   define BUF_SIZE 10
#endif

#ifndef TIME_AVG_OF
#   define TIME_AVG_OF 1000
#endif

typedef struct {
    double mean;
    double min;
    double max;
} timestat_t;

timestat_t getstat(double *times, int count)
{
    timestat_t stat = {
        .mean = 0,
        .min = DBL_MAX,
        .max = DBL_MIN
    };

    for(int i = 0; i < count; ++i) {
        stat.mean += times[i];
        stat.max = max(stat.max, times[i]);
        stat.min = min(stat.min, times[i]);
    }
    stat.mean /= count;
    return stat;
}

int main(int argc, char *argv[]) MPI_MAIN_TAG(MPI_ERRORS_ARE_FATAL,
{
    { // MPI_Bcast
        double times[TIME_AVG_OF] = {};

        int array[BUF_SIZE] = {};
        if(0 == mpi_rank) {
            for(int i = 0; i < BUF_SIZE; ++i) {
                array[i] = i;
            }
        }
        
        for(int i = 0; i < TIME_AVG_OF; ++i) {
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] -= MPI_Wtime();
            MPI_Bcast(array, BUF_SIZE, MPI_INT, 0, MPI_COMM_WORLD);
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] += MPI_Wtime();
        }
        if(0 == mpi_rank) {
            timestat_t stat = getstat(times, TIME_AVG_OF);
            printf("MPI_Bcast  : time elapsed (wtick = %lf): mean %lf min %lf max %lf\n", 
                                            MPI_Wtick(), stat.mean, stat.min, stat.max);
        }
#ifdef OUTPUT_RESULTS
        printf("MPI_Bcast(%d): ", mpi_rank);
        for(int i = 0; i < BUF_SIZE; ++i) {
            printf(" %d", array[i]);
        }
        printf("\n");
#endif // OUTPUT_RESULTS
    } // \MPI_Bcast

    { // MPI_Reduce
        double times[TIME_AVG_OF] = {};

        int array[BUF_SIZE] = {};
        for(int i = 0; i < BUF_SIZE; ++i) {
            array[i] = mpi_rank + i;
        }

        int result[BUF_SIZE] = {};
        for(int i = 0; i < TIME_AVG_OF; ++i) {
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] -= MPI_Wtime();
            MPI_Reduce(array, result, BUF_SIZE, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] += MPI_Wtime();
        }
        if(0 == mpi_rank) {
            timestat_t stat = getstat(times, TIME_AVG_OF);
            printf("MPI_Reduce : time elapsed (wtick = %lf): mean %lf min %lf max %lf\n", 
                                            MPI_Wtick(), stat.mean, stat.min, stat.max);
#ifdef OUTPUT_RESULTS
            printf("Reduce result {");
            for(int i = 0; i < BUF_SIZE; ++i) {
                printf(" %d", result[i]);
            }
            printf(" }\n");
#endif // OUTPUT_RESULTS
        }
    } // \MPI_Reduce

    { // MPI_Scatter
        double times[TIME_AVG_OF] = {};

        int send_buf[BUF_SIZE * mpi_size];
        if(0 == mpi_rank) {
            for(int i = 0; i < sizeof(send_buf) / sizeof(int); ++i) {
                send_buf[i] = i;
            }
        }
        int recv_buf[BUF_SIZE] = {};

        for(int i = 0; i < TIME_AVG_OF; ++i) {
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] -= MPI_Wtime();
            MPI_Scatter(send_buf, BUF_SIZE, MPI_INT, recv_buf, BUF_SIZE, MPI_INT, 0, MPI_COMM_WORLD);
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] += MPI_Wtime();
        }
        if(0 == mpi_rank) {
            timestat_t stat = getstat(times, TIME_AVG_OF);
            printf("MPI_Scatter: time elapsed (wtick = %lf): mean %lf min %lf max %lf\n", 
                                            MPI_Wtick(), stat.mean, stat.min, stat.max);
        }
#ifdef OUTPUT_RESULTS
        printf("MPI_Scatter(%d): ", mpi_rank);
        for(int i = 0; i < BUF_SIZE; ++i) {
            printf(" %d", recv_buf[i]);
        }
        printf("\n");
#endif // OUTPUT_RESULTS
    } // \MPI_Scatter

    { // MPI_Gather
        double times[TIME_AVG_OF] = {};

        int send_buf[BUF_SIZE] = {};
        for(int i = 0; i < sizeof(send_buf) / sizeof(int); ++i) {
            send_buf[i] = mpi_rank + i;
        }
        int recv_buf[BUF_SIZE * mpi_size];

        for(int i = 0; i < TIME_AVG_OF; ++i) {
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] -= MPI_Wtime();
            MPI_Gather(send_buf, BUF_SIZE, MPI_INT, recv_buf, BUF_SIZE, MPI_INT, 0, MPI_COMM_WORLD);
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] += MPI_Wtime();
        }
        if(0 == mpi_rank) {
            timestat_t stat = getstat(times, TIME_AVG_OF);
            printf("MPI_Gather : time elapsed (wtick = %lf): mean %lf min %lf max %lf\n", 
                                            MPI_Wtick(), stat.mean, stat.min, stat.max);
#ifdef OUTPUT_RESULTS
            printf("MPI_Gather: {");
            for(int i = 0; i < BUF_SIZE * mpi_size; ++i) {
                printf(" %d", recv_buf[i]);
            }
            printf(" }\n");
#endif // OUTPUT_RESULTS
        }
    } // \MPI_Gather
})
